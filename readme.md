### Init project:
```pip install -r requirements.txt```

### To run script please provide command line arguments:
- `-f` - format one of: mp3 or mp4
- `-d` - directory where to download files
- and one of below:
  - `-u` - Url to youtube video
  - `-pu` - Url to playlist

example:

```python .\downloader.py -u https://www.youtube.com/playlist?list=PLRomwVsNGznFggOY71VFRH_Wi8IOXj3hb -d music -f mp4```
