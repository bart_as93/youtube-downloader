import argparse
import os
from pytube import Playlist
from pytube import YouTube
import re
import unicodedata

def slugify(value, allow_unicode=False):
    """
    Taken from https://github.com/django/django/blob/master/django/utils/text.py
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize('NFKC', value)
    else:
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value.lower())
    return re.sub(r'[-\s]+', '-', value).strip('-_')

def downloadByVideo(vid):
    try:
        print('downloading : {} - {} with url : {}'.format(video.author, video.title, video.watch_url))
        global type
    
        vidFileName = slugify(vid.author + " - " + vid.title) + "." + format
        if format == 'mp3':
            type = 'audio'
            vid.streams. \
                filter(only_audio=True).desc().first(). \
                download(output_path=DOWNLOAD_DIR, filename=vidFileName)
        elif format == 'mp4':
            type = 'video'
            vid.streams. \
                filter(type=type, progressive=True, file_extension=format). \
                order_by('resolution'). \
                desc(). \
                first(). \
                download(output_path=DOWNLOAD_DIR, filename=vidFileName)
        else:
            print(f"Not supported format type: {format}")
            exit()
    except Exception as error:
        print(f"Something went wrong when downloading url: {video.watch_url}")
        print("Error: ", error)

parser = argparse.ArgumentParser()

# Adding optional argument
parser.add_argument("-u", "--Url", help="Youtube video url")
parser.add_argument("-pu", "--PlayListUrl", help="Playlist url")
parser.add_argument("-f", "--Format", help="Format of downloaded files - mp3 or mp4")
parser.add_argument("-d", "--Destination", help="Destination folder of downloaded files")

format="mp3"
# Read arguments from command line
args = parser.parse_args()

if not args.Url and not args.PlayListUrl:
    print("Please provide youtube playlist url via -pu or --PlayListUrl argument param or Youtube video url via -u or --Url")
    exit()
if not args.Destination:
    print("Please provide destination folder name via -d or --Destination argument param")
    exit()
if not args.Format:
    print("Format (-f or --Format not provided - default set to mp3")
else:
    format=args.Format

DOWNLOAD_DIR = os.getcwd() + "\\out\\" + args.Destination

if(args.PlayListUrl):
    playlist = Playlist(args.PlayListUrl)
    print (f"Downloading {len(playlist.videos)} files from playlist with url: {args.Url}, with format: {args.Format}, to dir: {DOWNLOAD_DIR}")

    for video in playlist.videos:
        downloadByVideo(video)

if (args.Url):
    video = YouTube(args.Url)
    downloadByVideo(video)
